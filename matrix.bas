OPTION _EXPLICIT
OPTION BASE 1
CONST MATRIX_INT_SIZE% = 2

matrix_run_tests

SUB matrix_run_tests
    DIM m AS Matrix

    'pre-initialization
    assert m.Rows = 0, "pre-init: rows != 0"
    assert m.Columns = 0, "pre-init: columns != 0"
    assert m.Initialized = 0, "pre-init: initialized is non-false"
    assert NOT matrix_is_valid(m), "pre-init: matrix should not be valid yet"
    assert matrix_is_zero(m), "pre-init: matrix reporting as non-zero"
    PRINT "Matrix pre-init: PASS"

    'initialization
    matrix_init m, 1, 1
    assert matrix_is_valid(m), "init: 1,1 matrix reporting invalid"
    assert matrix_is_zero(m), "init: 1,1 matrix reporting non-zero"
    assert m.Rows = 1, "init: 1,1 matrix wrong row count"
    assert m.Columns = 1, "init: 1,1 matrix wrong column count"
    matrix_init m, 100, 100
    assert matrix_is_valid(m), "init: 100,100 matrix reporting invalid"
    assert matrix_is_zero(m), "init: 100,100 matrix reporting non-zero"
    assert m.Rows = 100, "init: 100,100 matrix wrong row count"
    assert m.Columns = 100, "init: 100,100 matrix wrong column count"
    PRINT "Matrix init: PASS"

    'destruction
    matrix_free m
    assert NOT matrix_is_valid(m), "free: matrix should not be valid yet"
    assert matrix_is_zero(m), "free: matrix reporting as non-zero"
    PRINT "Matrix destruction: PASS"

    'element access
    matrix_init m, 1, 1
    matrix_set m, 1, 1, 42
    assert matrix_is_valid(m), "access: 1,1 matrix reporting invalid"
    assert NOT matrix_is_zero(m), "access: 1,1 matrix reporting zero"
    assert matrix_get(m, 1, 1) = 42, "access: 1,1 matrix failed read"
    matrix_init m, 100, 100
    matrix_set m, 42, 69, 42
    matrix_set m, 1, 1, 42
    assert matrix_is_valid(m), "access: 100,100 matrix reporting invalid"
    assert NOT matrix_is_zero(m), "access: 100,100 matrix reporting zero"
    assert matrix_get(m, 42, 69) = 42, "access: 100,100 matrix failed"
    assert matrix_get(m, 1, 1) = 42, "access: 100,100 matrix failed"
    PRINT "Matrix element access: PASS"

    'clear
    matrix_clear m
    assert matrix_is_valid(m), "clear: matrix reporting invalid"
    assert matrix_is_zero(m), "clear: matrix reporting non-zero"
    assert matrix_get(m, 42, 69) = 0, "clear: 100,100 matrix failed"
    assert matrix_get(m, 1, 1) = 0, "clear: 100,100 matrix failed"
    PRINT "Matrix clear: PASS"

    'cleanup
    matrix_free m
END SUB


'ACTUAL CODE
TYPE Matrix
    Rows AS INTEGER
    Columns AS INTEGER
    RawData AS _MEM
    Initialized AS _BYTE
END TYPE

SUB matrix_init (matrix AS Matrix, rows AS INTEGER, columns AS INTEGER)
    assert rows > 0, "Invalid row count"
    assert columns > 0, "Invalid column count"
    assert rows * columns < 500000, "Your computer would catch fire with a matrix that size!"
    IF matrix.Initialized = -1 THEN _MEMFREE matrix.RawData
    CALL matrix_private_alloc(rows, columns, matrix, -1)
END SUB

SUB matrix_free (matrix AS Matrix)
    IF matrix.Initialized = -1 THEN _MEMFREE matrix.RawData
    matrix.Initialized = 0
END SUB

SUB matrix_clear (matrix AS Matrix)
    _MEMFILL matrix.RawData, matrix.RawData.OFFSET, matrix.RawData.SIZE, 0 AS _BYTE
END SUB

FUNCTION matrix_get (matrix AS Matrix, row AS INTEGER, column AS INTEGER)
    assert matrix.Initialized, "Matrix not yet initialized, use matrix_init <matrix>,<rows>,<columns>"
    assert row > 0 AND row <= matrix.Rows, "Row out of bounds"
    assert column > 0 AND column <= matrix.Columns, "Column out of bounds"
    assert row * column < matrix.RawData.SIZE / MATRIX_INT_SIZE, "matrix_get: Internal matrix data error, probably not your fault"
    DIM ret AS INTEGER
    _MEMGET matrix.RawData, matrix.RawData.OFFSET + (row * column * MATRIX_INT_SIZE), ret
    matrix_get = ret
END FUNCTION

SUB matrix_set (matrix AS Matrix, row AS INTEGER, column AS INTEGER, value AS INTEGER)
    assert matrix.Initialized, "Matrix not yet initialized, use matrix_init <matrix>,<rows>,<columns>"
    assert row > 0 AND row <= matrix.Rows, "Row out of bounds"
    assert column > 0 AND column <= matrix.Columns, "Column out of bounds"
    assert row * column < matrix.RawData.SIZE / MATRIX_INT_SIZE, "matrix_set: Internal matrix data error, probably not your fault"
    _MEMPUT matrix.RawData, matrix.RawData.OFFSET + (row * column * MATRIX_INT_SIZE), value
END SUB

FUNCTION matrix_is_valid (matrix AS Matrix)
    IF matrix.Initialized <> -1 THEN
        matrix_is_valid = 0
    ELSEIF matrix.Rows < 1 OR matrix.Columns < 1 THEN
        matrix_is_valid = 0
    ELSE
        matrix_is_valid = -1
    END IF
END FUNCTION

FUNCTION matrix_is_zero (matrix AS Matrix)
    DIM ret AS INTEGER
    ret = -1
    IF matrix_is_valid(matrix) THEN
        DIM i, v AS INTEGER
        FOR i = 1 TO (matrix.Rows * matrix.Columns * MATRIX_INT_SIZE)
            _MEMGET matrix.RawData, matrix.RawData.OFFSET + i, v
            IF v <> 0 THEN
                ret = 0
                EXIT FOR
            END IF
        NEXT i
    END IF
    matrix_is_zero = ret
END FUNCTION

SUB matrix_identiy (matrix AS Matrix, size AS INTEGER)
    matrix_init matrix, size, size
    DIM i AS INTEGER
    FOR i = 1 TO size
        matrix_set matrix, i, i, 1
    NEXT i
END SUB

SUB matrix_fill (matrix AS Matrix, value AS INTEGER)
    _MEMFILL matrix.RawData, matrix.RawData.OFFSET, matrix.RawData.SIZE, value
END SUB

SUB matrix_private_alloc (rows AS INTEGER, columns AS INTEGER, ret AS Matrix, zero AS _BYTE)
    ret.Rows = rows
    ret.Columns = columns
    ret.RawData = _MEMNEW(rows * columns * MATRIX_INT_SIZE + 32) 'add 32 bits padding
    IF zero = -1 THEN
        _MEMFILL ret.RawData, ret.RawData.OFFSET, ret.RawData.SIZE, 0 AS _BYTE
    END IF
    ret.Initialized = -1
END SUB

SUB assert (condition AS _BYTE, message AS STRING)
    IF condition = 0 THEN
        PRINT message
        END
    END IF
END SUB
